﻿namespace FruitDealer.Domain
{
    public class FruitEater
    {
        public int Id { get; set; }

        public int FruitId { get; set; }
        public Fruit Fruit { get; set; }

        public int EaterId { get; set; }
        public Eater Eater { get; set; }
    }
}