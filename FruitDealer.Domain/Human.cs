﻿using System.Collections.Generic;

namespace FruitDealer.Domain
{
    public class Human
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Economy Economy { get; set; }

        public Eater Eater { get; set; }
    }

    public enum Economy
    {
        Rich,
        Poor
    }
}