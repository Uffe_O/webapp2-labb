﻿namespace FruitDealer.Domain
{
    public class Dealer
    {
        public int Id { get; set; }
        public int FruitsLoaned { get; set; }
        public string Name { get; set; }

        public int StoreId { get; set; }
        public Store Store { get; set; }

        public int HumanId { get; set; }
        public Human Human { get; set; }
    }
}