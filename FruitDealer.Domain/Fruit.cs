﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FruitDealer.Domain
{
    public class Fruit
    {
        public Fruit()
        {
            FruitEaters = new List<FruitEater>();
        }

        public int Id { get; set; }
        public string Color { get; set; }
        public DateTime BadDate { get; set; }
        public DateTime BoughtDate { get; set; }
        public FruitTypes FruitTypes { get; set; }

        public int StoreId { get; set; }
        public Store Store { get; set; }

        public List<FruitEater> FruitEaters { get; set; }
    }

    public enum FruitTypes
    {
        Apple,
        Orange,
        Banana,
        Pear
    }
}