﻿using System.Collections.Generic;

namespace FruitDealer.Domain
{
    public class Eater
    {
        public Eater()
        {
            FruitEaters = new List<FruitEater>();
        }

        public int Id { get; set; }
        public bool Healthy { get; set; }

        public int HumanId { get; set; }
        public Human Human { get; set; }

        public List<FruitEater> FruitEaters { get; set; }
    }
}