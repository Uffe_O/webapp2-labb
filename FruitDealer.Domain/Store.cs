﻿using System.Collections.Generic;

namespace FruitDealer.Domain
{
    public class Store
    {
        public Store()
        {
            Dealers = new List<Dealer>();
            Fruits = new List<Fruit>();
        }

        public int Id { get; set; }
        public string StoreName { get; set; }

        public List<Dealer> Dealers { get; set; }
        public List<Fruit> Fruits { get; set; }
    }
}