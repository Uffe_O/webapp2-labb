﻿using FruitDealer.Domain;
using Microsoft.EntityFrameworkCore;

namespace FruitDealer.Data
{
    public class FruitDealerContext : DbContext
    {
        public DbSet<Dealer> Dealers { get; set; }
        public DbSet<Eater> Eaters { get; set; }
        public DbSet<Fruit> Fruits { get; set; }
        public DbSet<FruitEater> FruitEaters { get; set; }
        public DbSet<Human> Humans { get; set; }
        public DbSet<Store> Stores { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //modelBuilder.Entity<FruitEater>()
            //    .HasKey(f => new { f.EaterId, f.FruitId });

            ////modelBuilder.Entity<Human>()
            ////    .Property(h => h.Eater).IsRequired();

            //modelBuilder.Entity<Fruit>(entity =>
            //{
            //    entity.Property(d => d.BoughtDate).HasDefaultValueSql("'0001-01-01T00:00:00.000'");
            //    entity.Property(d => d.BadDate).HasDefaultValueSql("'0001-01-01T00:00:00.000'");
            //});
                
            base.OnModelCreating(modelBuilder);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(
          "Server = (localdb)\\mssqllocaldb; Database = FruitDealer; Trusted_Connection = True; ");
        }
    }
}