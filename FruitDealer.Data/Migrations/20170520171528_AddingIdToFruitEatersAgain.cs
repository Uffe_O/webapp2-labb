﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace FruitDealer.Data.Migrations
{
    public partial class AddingIdToFruitEatersAgain : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_FruitEaters",
                table: "FruitEaters");

            migrationBuilder.AddColumn<int>(
                name: "Id",
                table: "FruitEaters",
                nullable: false,
                defaultValue: 0)
                .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            migrationBuilder.AddPrimaryKey(
                name: "PK_FruitEaters",
                table: "FruitEaters",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_FruitEaters_EaterId",
                table: "FruitEaters",
                column: "EaterId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_FruitEaters",
                table: "FruitEaters");

            migrationBuilder.DropIndex(
                name: "IX_FruitEaters_EaterId",
                table: "FruitEaters");

            migrationBuilder.DropColumn(
                name: "Id",
                table: "FruitEaters");

            migrationBuilder.AddPrimaryKey(
                name: "PK_FruitEaters",
                table: "FruitEaters",
                columns: new[] { "EaterId", "FruitId" });
        }
    }
}
