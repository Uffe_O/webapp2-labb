﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace FruitDealer.Data.Migrations
{
    public partial class RemovedAnimalClasses : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Eaters_SmartAnimals_SmartAnimalId",
                table: "Eaters");

            migrationBuilder.DropForeignKey(
                name: "FK_Humans_Eaters_EaterId",
                table: "Humans");

            migrationBuilder.DropTable(
                name: "SmartAnimals");

            migrationBuilder.DropTable(
                name: "Animals");

            migrationBuilder.DropIndex(
                name: "IX_Humans_EaterId",
                table: "Humans");

            migrationBuilder.DropIndex(
                name: "IX_Eaters_SmartAnimalId",
                table: "Eaters");

            migrationBuilder.DropColumn(
                name: "FruitsForSale",
                table: "Stores");

            migrationBuilder.DropColumn(
                name: "EaterId",
                table: "Humans");

            migrationBuilder.RenameColumn(
                name: "SmartAnimalId",
                table: "Eaters",
                newName: "HumanId");

            migrationBuilder.AddColumn<int>(
                name: "Fruits",
                table: "Fruits",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "HumanId",
                table: "Dealers",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Eaters_HumanId",
                table: "Eaters",
                column: "HumanId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Dealers_HumanId",
                table: "Dealers",
                column: "HumanId");

            migrationBuilder.AddForeignKey(
                name: "FK_Dealers_Humans_HumanId",
                table: "Dealers",
                column: "HumanId",
                principalTable: "Humans",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Eaters_Humans_HumanId",
                table: "Eaters",
                column: "HumanId",
                principalTable: "Humans",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Dealers_Humans_HumanId",
                table: "Dealers");

            migrationBuilder.DropForeignKey(
                name: "FK_Eaters_Humans_HumanId",
                table: "Eaters");

            migrationBuilder.DropIndex(
                name: "IX_Eaters_HumanId",
                table: "Eaters");

            migrationBuilder.DropIndex(
                name: "IX_Dealers_HumanId",
                table: "Dealers");

            migrationBuilder.DropColumn(
                name: "Fruits",
                table: "Fruits");

            migrationBuilder.DropColumn(
                name: "HumanId",
                table: "Dealers");

            migrationBuilder.RenameColumn(
                name: "HumanId",
                table: "Eaters",
                newName: "SmartAnimalId");

            migrationBuilder.AddColumn<int>(
                name: "FruitsForSale",
                table: "Stores",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "EaterId",
                table: "Humans",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Animals",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Animals = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Animals", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SmartAnimals",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AnimalId = table.Column<int>(nullable: false),
                    HumanId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SmartAnimals", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SmartAnimals_Animals_AnimalId",
                        column: x => x.AnimalId,
                        principalTable: "Animals",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SmartAnimals_Humans_HumanId",
                        column: x => x.HumanId,
                        principalTable: "Humans",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Humans_EaterId",
                table: "Humans",
                column: "EaterId");

            migrationBuilder.CreateIndex(
                name: "IX_Eaters_SmartAnimalId",
                table: "Eaters",
                column: "SmartAnimalId");

            migrationBuilder.CreateIndex(
                name: "IX_SmartAnimals_AnimalId",
                table: "SmartAnimals",
                column: "AnimalId");

            migrationBuilder.CreateIndex(
                name: "IX_SmartAnimals_HumanId",
                table: "SmartAnimals",
                column: "HumanId");

            migrationBuilder.AddForeignKey(
                name: "FK_Eaters_SmartAnimals_SmartAnimalId",
                table: "Eaters",
                column: "SmartAnimalId",
                principalTable: "SmartAnimals",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Humans_Eaters_EaterId",
                table: "Humans",
                column: "EaterId",
                principalTable: "Eaters",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
