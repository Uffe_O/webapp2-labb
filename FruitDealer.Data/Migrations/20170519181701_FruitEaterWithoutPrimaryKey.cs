﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace FruitDealer.Data.Migrations
{
    public partial class FruitEaterWithoutPrimaryKey : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_FruitEaters",
                table: "FruitEaters");

            migrationBuilder.DropIndex(
                name: "IX_FruitEaters_EaterId",
                table: "FruitEaters");

            migrationBuilder.DropColumn(
                name: "Id",
                table: "FruitEaters");

            migrationBuilder.DropColumn(
                name: "Healthy",
                table: "Fruits");

            migrationBuilder.DropColumn(
                name: "Foods",
                table: "Eaters");

            migrationBuilder.AddColumn<int>(
                name: "Quantity",
                table: "Fruits",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<bool>(
                name: "Healthy",
                table: "Eaters",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddPrimaryKey(
                name: "PK_FruitEaters",
                table: "FruitEaters",
                columns: new[] { "EaterId", "FruitId" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_FruitEaters",
                table: "FruitEaters");

            migrationBuilder.DropColumn(
                name: "Quantity",
                table: "Fruits");

            migrationBuilder.DropColumn(
                name: "Healthy",
                table: "Eaters");

            migrationBuilder.AddColumn<int>(
                name: "Id",
                table: "FruitEaters",
                nullable: false,
                defaultValue: 0)
                .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            migrationBuilder.AddColumn<bool>(
                name: "Healthy",
                table: "Fruits",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "Foods",
                table: "Eaters",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddPrimaryKey(
                name: "PK_FruitEaters",
                table: "FruitEaters",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_FruitEaters_EaterId",
                table: "FruitEaters",
                column: "EaterId");
        }
    }
}
