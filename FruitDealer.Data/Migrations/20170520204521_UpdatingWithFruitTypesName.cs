﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FruitDealer.Data.Migrations
{
    public partial class UpdatingWithFruitTypesName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Fruits",
                table: "Fruits");

            migrationBuilder.AddColumn<int>(
                name: "FruitTypes",
                table: "Fruits",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FruitTypes",
                table: "Fruits");

            migrationBuilder.AddColumn<int>(
                name: "Fruits",
                table: "Fruits",
                nullable: false,
                defaultValue: 0);
        }
    }
}
