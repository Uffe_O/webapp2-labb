﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FruitDealer.Data.Migrations
{
    public partial class RemovingFruitsFromHuman : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Fruits_Humans_HumanId",
                table: "Fruits");

            migrationBuilder.DropIndex(
                name: "IX_Fruits_HumanId",
                table: "Fruits");

            migrationBuilder.DropColumn(
                name: "HumanId",
                table: "Fruits");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "HumanId",
                table: "Fruits",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Fruits_HumanId",
                table: "Fruits",
                column: "HumanId");

            migrationBuilder.AddForeignKey(
                name: "FK_Fruits_Humans_HumanId",
                table: "Fruits",
                column: "HumanId",
                principalTable: "Humans",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
