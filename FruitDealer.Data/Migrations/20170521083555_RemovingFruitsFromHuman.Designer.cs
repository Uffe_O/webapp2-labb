﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using FruitDealer.Data;
using FruitDealer.Domain;

namespace FruitDealer.Data.Migrations
{
    [DbContext(typeof(FruitDealerContext))]
    [Migration("20170521083555_RemovingFruitsFromHuman")]
    partial class RemovingFruitsFromHuman
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.2")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("FruitDealer.Domain.Dealer", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("FruitsLoaned");

                    b.Property<int>("HumanId");

                    b.Property<string>("Name");

                    b.Property<int>("StoreId");

                    b.HasKey("Id");

                    b.HasIndex("HumanId");

                    b.HasIndex("StoreId");

                    b.ToTable("Dealers");
                });

            modelBuilder.Entity("FruitDealer.Domain.Eater", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("Healthy");

                    b.Property<int>("HumanId");

                    b.HasKey("Id");

                    b.HasIndex("HumanId")
                        .IsUnique();

                    b.ToTable("Eaters");
                });

            modelBuilder.Entity("FruitDealer.Domain.Fruit", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("BadDate");

                    b.Property<DateTime>("BoughtDate");

                    b.Property<string>("Color");

                    b.Property<int>("FruitTypes");

                    b.Property<int>("StoreId");

                    b.HasKey("Id");

                    b.HasIndex("StoreId");

                    b.ToTable("Fruits");
                });

            modelBuilder.Entity("FruitDealer.Domain.FruitEater", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("EaterId");

                    b.Property<int>("FruitId");

                    b.HasKey("Id");

                    b.HasIndex("EaterId");

                    b.HasIndex("FruitId");

                    b.ToTable("FruitEaters");
                });

            modelBuilder.Entity("FruitDealer.Domain.Human", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("Economy");

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("Humans");
                });

            modelBuilder.Entity("FruitDealer.Domain.Store", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("StoreName");

                    b.HasKey("Id");

                    b.ToTable("Stores");
                });

            modelBuilder.Entity("FruitDealer.Domain.Dealer", b =>
                {
                    b.HasOne("FruitDealer.Domain.Human", "Human")
                        .WithMany()
                        .HasForeignKey("HumanId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("FruitDealer.Domain.Store", "Store")
                        .WithMany("Dealers")
                        .HasForeignKey("StoreId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("FruitDealer.Domain.Eater", b =>
                {
                    b.HasOne("FruitDealer.Domain.Human", "Human")
                        .WithOne("Eater")
                        .HasForeignKey("FruitDealer.Domain.Eater", "HumanId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("FruitDealer.Domain.Fruit", b =>
                {
                    b.HasOne("FruitDealer.Domain.Store", "Store")
                        .WithMany("Fruits")
                        .HasForeignKey("StoreId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("FruitDealer.Domain.FruitEater", b =>
                {
                    b.HasOne("FruitDealer.Domain.Eater", "Eater")
                        .WithMany("FruitEaters")
                        .HasForeignKey("EaterId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("FruitDealer.Domain.Fruit", "Fruit")
                        .WithMany("FruitEaters")
                        .HasForeignKey("FruitId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
