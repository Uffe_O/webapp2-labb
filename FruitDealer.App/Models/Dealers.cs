﻿namespace FruitDealer.App.Models
{
    public partial class Dealers
    {
        public int Id { get; set; }
        public int FruitsLoaned { get; set; }
        public string Name { get; set; }
        public int StoreId { get; set; }
        public int HumanId { get; set; }

        public virtual Humans Human { get; set; }
        public virtual Stores Store { get; set; }
    }
}
