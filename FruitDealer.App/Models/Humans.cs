﻿using System.Collections.Generic;
using System.ComponentModel;

namespace FruitDealer.App.Models
{
    public partial class Humans
    {
        public Humans()
        {
            Dealers = new HashSet<Dealers>();
        }

        public int Id { get; set; }
        public Economy Economy { get; set; }
        public string Name { get; set; }

        [DisplayName("Eaters")]
        public virtual Eaters Eaters { get; set; }
        [DisplayName("Dealers")]
        public virtual ICollection<Dealers> Dealers { get; set; }
    }

    public enum Economy
    {
        Rich,
        Poor
    }
}
