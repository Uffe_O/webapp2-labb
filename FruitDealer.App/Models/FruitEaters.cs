﻿using System.ComponentModel;

namespace FruitDealer.App.Models
{
    public partial class FruitEaters
    {
        [DisplayName("Eater")]
        public int EaterId { get; set; }
        [DisplayName("Fruit")]
        public int FruitId { get; set; }
        public int Id { get; set; }

        public virtual Eaters Eater { get; set; }
        public virtual Fruits Fruit { get; set; }
    }
}
