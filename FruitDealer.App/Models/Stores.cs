﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace FruitDealer.App.Models
{
    public partial class Stores
    {
        public Stores()
        {
            Dealers = new HashSet<Dealers>();
            Fruits = new HashSet<Fruits>();
        }

        public int Id { get; set; }
        [StringLength(50, MinimumLength = 2)]
        [RegularExpression("([a-zA-Z0-9 .&'-]+)", ErrorMessage = "The field Name should only include letters and number.")]
        [Required]
        public string StoreName { get; set; }

        [DisplayName("Dealers")]
        public virtual ICollection<Dealers> Dealers { get; set; }
        [DisplayName("Fruits in store")]
        public virtual ICollection<Fruits> Fruits { get; set; }
    }
}
