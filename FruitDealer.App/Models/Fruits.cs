﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace FruitDealer.App.Models
{
    public partial class Fruits
    {
        public Fruits()
        {
            FruitEaters = new HashSet<FruitEaters>();
        }

        public int Id { get; set; }
        [CustomDateRange]
        public DateTime BadDate { get; set; }
        [CustomDateRange]
        public DateTime BoughtDate { get; set; }
        public string Color { get; set; }
        public int StoreId { get; set; }
        public FruitTypes FruitTypes { get; set; }

        [DisplayName("Fruit eaters")]
        public virtual ICollection<FruitEaters> FruitEaters { get; set; }
        [DisplayName("Store")]
        public virtual Stores Store { get; set; }

        public class CustomDateRangeAttribute : RangeAttribute
        {
            public CustomDateRangeAttribute()
              : base(typeof(DateTime),
                      new DateTime(1750, 01, 01).ToString(),
                      DateTime.Now.Date.ToString())
            { }
        }
    }

    public enum FruitTypes
    {
        Apple,
        Orange,
        Banana,
        Pear
    }
}
