﻿using System.Collections.Generic;
using System.ComponentModel;

namespace FruitDealer.App.Models
{
    public partial class Eaters
    {
        public Eaters()
        {
            FruitEaters = new HashSet<FruitEaters>();
        }

        public int Id { get; set; }
        public int HumanId { get; set; }
        public bool Healthy { get; set; }

        [DisplayName("Fruit eaters")]
        public virtual ICollection<FruitEaters> FruitEaters { get; set; }
        public virtual Humans Human { get; set; }
    }
}
