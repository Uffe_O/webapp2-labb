﻿using Microsoft.EntityFrameworkCore;

namespace FruitDealer.App.Models
{
    public partial class EfFruitDealerContext : DbContext
    {
        public virtual DbSet<Dealers> Dealers { get; set; }
        public virtual DbSet<Eaters> Eaters { get; set; }
        public virtual DbSet<FruitEaters> FruitEaters { get; set; }
        public virtual DbSet<Fruits> Fruits { get; set; }
        public virtual DbSet<Humans> Humans { get; set; }
        public virtual DbSet<Stores> Stores { get; set; }

        public EfFruitDealerContext(DbContextOptions<EfFruitDealerContext> options) : base(options)
        {

        }

        public EfFruitDealerContext()
        {
        }

        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    optionsBuilder.UseSqlServer(@"Server = (localdb)\mssqllocaldb; Database = FruitDealer; Trusted_Connection = True;");
        //}

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Dealers>(entity =>
            {
                entity.HasIndex(e => e.HumanId)
                    .HasName("IX_Dealers_HumanId");

                entity.HasIndex(e => e.StoreId)
                    .HasName("IX_Dealers_StoreId");

                entity.Property(e => e.HumanId).HasDefaultValueSql("0");

                entity.HasOne(d => d.Human)
                    .WithMany(p => p.Dealers)
                    .HasForeignKey(d => d.HumanId);

                entity.HasOne(d => d.Store)
                    .WithMany(p => p.Dealers)
                    .HasForeignKey(d => d.StoreId);
            });

            modelBuilder.Entity<Eaters>(entity =>
            {
                entity.HasIndex(e => e.HumanId)
                    .HasName("IX_Eaters_HumanId")
                    .IsUnique();

                entity.Property(e => e.Healthy).HasDefaultValueSql("0");

                entity.HasOne(d => d.Human)
                    .WithOne(p => p.Eaters)
                    .HasForeignKey<Eaters>(d => d.HumanId);
            });

            modelBuilder.Entity<FruitEaters>(entity =>
            {
                entity.HasIndex(e => e.EaterId)
                    .HasName("IX_FruitEaters_EaterId");

                entity.HasIndex(e => e.FruitId)
                    .HasName("IX_FruitEaters_FruitId");

                entity.HasOne(d => d.Eater)
                    .WithMany(p => p.FruitEaters)
                    .HasForeignKey(d => d.EaterId);

                entity.HasOne(d => d.Fruit)
                    .WithMany(p => p.FruitEaters)
                    .HasForeignKey(d => d.FruitId);
            });

            modelBuilder.Entity<Fruits>(entity =>
            {
                entity.HasIndex(e => e.StoreId)
                    .HasName("IX_Fruits_StoreId");

                entity.Property(e => e.FruitTypes).HasDefaultValueSql("0");
                
                entity.HasOne(d => d.Store)
                    .WithMany(p => p.Fruits)
                    .HasForeignKey(d => d.StoreId);

                entity.Property(d => d.BoughtDate).HasDefaultValueSql("'0001-01-01T00:00:00.000'");
                entity.Property(d => d.BadDate).HasDefaultValueSql("'0001-01-01T00:00:00.000'");
            });
        }
    }
}