﻿using System;
using FruitDealer.Data;
using FruitDealer.Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using FruitDealer.App.Models;
using Economy = FruitDealer.Domain.Economy;
using FruitTypes = FruitDealer.Domain.FruitTypes;

namespace FruitDealer.App.Controllers
{
    public class HomeController : Controller
    {
        private readonly EfFruitDealerContext _context;

        public HomeController(EfFruitDealerContext context)
        {
            _context = context;
        }

        public IActionResult CreateDatabaseAction()
        {
            ClearDatabase();
            CreateDatabase();
            return RedirectToAction("Index", "Fruits");
        }

        public void CreateDatabase()
        {
            _context.Database.ExecuteSqlCommand(

                "SET IDENTITY_INSERT Stores ON " +
                "INSERT INTO [dbo].[Stores] ([Id], [StoreName]) VALUES (1, N'Frukt affär maffian')" +
                "INSERT INTO [dbo].[Stores] ([Id], [StoreName]) VALUES (2, N'3 for 1 shop')" +
                "INSERT INTO [dbo].[Stores] ([Id], [StoreName]) VALUES (3, N'Paper on buy store')" +
                "SET IDENTITY_INSERT Stores OFF " +

                "SET IDENTITY_INSERT [dbo].[Humans] ON " +
                "INSERT INTO [dbo].[Humans] ([Id], [Name], [Economy]) VALUES (1, N'TheZap', 0)" +
                "INSERT INTO [dbo].[Humans] ([Id], [Name], [Economy]) VALUES (2, N'TheWho', 0)" +
                "INSERT INTO [dbo].[Humans] ([Id], [Name], [Economy]) VALUES (3, N'TheRanaway', 1)" +
                "INSERT INTO [dbo].[Humans] ([Id], [Name], [Economy]) VALUES (4, N'TheSingbad', 1)" +
                "INSERT INTO [dbo].[Humans] ([Id], [Name], [Economy]) VALUES (5, N'TheWalker', 1)" +
                "SET IDENTITY_INSERT [dbo].[Humans] OFF " +

                "SET IDENTITY_INSERT [dbo].[Dealers] ON " +
                "INSERT INTO [dbo].[Dealers] ([Id], [Name], [FruitsLoaned], [StoreId], [HumanId]) VALUES (1, N'Seller', 10, 1 , 1)" +
                "INSERT INTO [dbo].[Dealers] ([Id], [Name], [FruitsLoaned], [StoreId], [HumanId]) VALUES (2, N'Seller2', 20, 2, 2)" +
                "INSERT INTO [dbo].[Dealers] ([Id], [Name], [FruitsLoaned], [StoreId], [HumanId]) VALUES (3, N'Dealer', 0, 3, 3)" +
                "SET IDENTITY_INSERT [dbo].[Dealers] OFF " +

                "SET IDENTITY_INSERT [dbo].[Fruits] ON " +
                "INSERT INTO [dbo].[Fruits] ([Id], [Color], [BoughtDate], [BadDate], [FruitTypes], [StoreId]) VALUES (1, N'Green', '2011-05-05 00:00:00', '2011-06-01 00:00:00', 0, 1)" +
                "INSERT INTO [dbo].[Fruits] ([Id], [Color], [BoughtDate], [BadDate], [FruitTypes], [StoreId]) VALUES (2, N'Green', '2011-06-05 00:00:00', '2011-07-01 00:00:00', 0, 1)" +
                "INSERT INTO [dbo].[Fruits] ([Id], [Color], [BoughtDate], [BadDate], [FruitTypes], [StoreId]) VALUES (3, N'Yellow', '2011-08-05 00:00:00', '2011-09-01 00:00:00', 2, 1)" +
                "INSERT INTO [dbo].[Fruits] ([Id], [Color], [BoughtDate], [BadDate], [FruitTypes], [StoreId]) VALUES (4, N'Yellow', '2012-05-05 00:00:00', '2012-06-06 00:00:00', 2, 1)" +
                "INSERT INTO [dbo].[Fruits] ([Id], [Color], [BoughtDate], [BadDate], [FruitTypes], [StoreId]) VALUES (5, N'Orange', '2012-04-05 00:00:00', '2012-04-10 00:00:00', 1, 2)" +
                "INSERT INTO [dbo].[Fruits] ([Id], [Color], [BoughtDate], [BadDate], [FruitTypes], [StoreId]) VALUES (6, N'Orange', '2012-05-10 00:00:00', '2012-06-01 00:00:00', 1, 2)" +
                "INSERT INTO [dbo].[Fruits] ([Id], [Color], [BoughtDate], [BadDate], [FruitTypes], [StoreId]) VALUES (7, N'Green', '2012-05-05 00:00:00', '2012-06-01 00:00:00', 3, 2)" +
                "INSERT INTO [dbo].[Fruits] ([Id], [Color], [BoughtDate], [BadDate], [FruitTypes], [StoreId]) VALUES (8, N'Green', '2012-05-05 00:00:00', '2012-06-01 00:00:00', 0, 3)" +
                "SET IDENTITY_INSERT [dbo].[Fruits] OFF " +

                "SET IDENTITY_INSERT [dbo].[Eaters] ON " +
                "INSERT INTO [dbo].[Eaters] ([Id], [Healthy], [HumanId]) VALUES (1, 0, 1)" +
                "INSERT INTO [dbo].[Eaters] ([Id], [Healthy], [HumanId]) VALUES (2, 0, 2)" +
                "INSERT INTO [dbo].[Eaters] ([Id], [Healthy], [HumanId]) VALUES (3, 0, 3)" +
                "INSERT INTO [dbo].[Eaters] ([Id], [Healthy], [HumanId]) VALUES (4, 1, 4)" +
                "INSERT INTO [dbo].[Eaters] ([Id], [Healthy], [HumanId]) VALUES (5, 1, 5)" +
                "SET IDENTITY_INSERT [dbo].[Eaters] OFF " +

                "SET IDENTITY_INSERT [dbo].[FruitEaters] ON " +
                "INSERT INTO [dbo].[FruitEaters] ([Id] ,[FruitId], [EaterId]) VALUES (1, 1, 4)" +
                "INSERT INTO [dbo].[FruitEaters] ([Id] ,[FruitId], [EaterId]) VALUES (2, 2, 4)" +
                "INSERT INTO [dbo].[FruitEaters] ([Id] ,[FruitId], [EaterId]) VALUES (3, 3, 4)" +
                "INSERT INTO [dbo].[FruitEaters] ([Id] ,[FruitId], [EaterId]) VALUES (4, 4, 4)" +
                "INSERT INTO [dbo].[FruitEaters] ([Id] ,[FruitId], [EaterId]) VALUES (5, 5, 5)" +
                "INSERT INTO [dbo].[FruitEaters] ([Id] ,[FruitId], [EaterId]) VALUES (6, 6, 5)" +
                "SET IDENTITY_INSERT [dbo].[FruitEaters] OFF "
            );

            //var humans = new List<Humans>
            //{
            //    new Humans
            //    {
            //        Id = 1, Eaters = new Eaters
            //        {
            //            Id = 1
            //        }, Name = "TheZap"
            //    },
            //    new Humans
            //    {
            //        Id = 1, Eaters = new Eaters
            //        {
            //            Id = 2
            //        }, Name = "TheWho"
            //    },
            //    new Humans
            //    {
            //        Id = 1, Eaters = new Eaters
            //        {
            //            Id = 3
            //        }, Name = "TheRanaway"
            //    },
            //    new Humans
            //    {
            //        Id = 1, Eaters = new Eaters
            //        {
            //            Id = 4
            //        }, Name = "TheSingBad"
            //    },
            //    new Humans
            //    {
            //        Id = 1, Eaters = new Eaters
            //        {
            //            Id = 5
            //        }, Name = "TheWalker"
            //    }
            //};

            //var dealers = new List<Dealers>
            //{
            //    new Dealers
            //    {
            //        Id = 1, HumanId = 1, FruitsLoaned = 10, Human = humans.FirstOrDefault(x => x.Id == 1)
            //    },
            //    new Dealers
            //    {
            //        Id = 2, HumanId = 2, FruitsLoaned = 20, Human = humans.FirstOrDefault(x => x.Id == 2)
            //    },
            //    new Dealers
            //    {
            //        Id = 3, HumanId = 3, FruitsLoaned = 0, Human = humans.FirstOrDefault(x => x.Id == 3)
            //    }
            //};

            //var fruits = new List<Fruits>
            //{
            //    new Fruits
            //    {
            //        Id = 1, BoughtDate = DateTime.Now, BadDate = DateTime.Now.AddDays(10), Color = "Yellow"
            //    }
            //};

            //var stores = new List<Stores>
            //{
            //    new Stores
            //    {
            //        Id = 1, Dealers = new List<Dealers>
            //        {
            //            dealers.FirstOrDefault(x => x.Id == 1), dealers.FirstOrDefault(x => x.Id == 2)
            //        }, Fruits = new List<Fruits>
            //        {
            //            fruits.FirstOrDefault(x => x.Id == 1)
            //        }
            //    }
            //};

            //var fruitEaters = new List<FruitEaters>
            //{
            //    new FruitEaters
            //    {
            //    }
            //};

            //using (var context = _context)
            //{
            //    context.Humans.AddRange(humans);
            //    context.Dealers.AddRange(dealers);
            //    context.Stores.AddRange(stores);
                
            //}

            _context.SaveChanges();
        }

        public IActionResult ClearDatabaseAction()
        {
            ClearDatabase();
            return RedirectToAction("Index", "Stores");
        }

        public void ClearDatabase()
        {
            var fruitEaters = _context.FruitEaters.Select(fe => fe);
            _context.FruitEaters.RemoveRange(fruitEaters);

            var eaters = _context.Eaters.Select(e => e);
            _context.Eaters.RemoveRange(eaters);

            var fruits = _context.Fruits.Select(f => f);
            _context.Fruits.RemoveRange(fruits);

            var dealers = _context.Dealers.Select(d => d);
            _context.Dealers.RemoveRange(dealers);

            var humans = _context.Humans.Select(h => h);
            _context.Humans.RemoveRange(humans);

            var stores = _context.Stores.Select(s => s);
            _context.Stores.RemoveRange(stores);

            _context.SaveChanges();
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}