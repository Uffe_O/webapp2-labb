using FruitDealer.App.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace FruitDealer.App.Controllers
{
    public class DealersController : Controller
    {
        private readonly EfFruitDealerContext _context;

        public DealersController(EfFruitDealerContext context)
        {
            _context = context;
        }

        // GET: Dealers
        public async Task<IActionResult> Index()
        {
            var fruitDealerContext = _context.Dealers.Include(d => d.Human).Include(d => d.Store);
            return View(await fruitDealerContext.ToListAsync());
        }

        // GET: Dealers/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var dealers = await _context.Dealers
                .Include(d => d.Human)
                .Include(d => d.Store)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (dealers == null)
            {
                return NotFound();
            }

            return View(dealers);
        }

        // GET: Dealers/Create
        public IActionResult Create()
        {
            ViewData["HumanId"] = new SelectList(_context.Humans, "Id", "Id");
            ViewData["StoreId"] = new SelectList(_context.Stores, "Id", "Id");
            return View();
        }

        // POST: Dealers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,FruitsLoaned,Name,StoreId,HumanId")] Dealers dealers)
        {
            if (ModelState.IsValid)
            {
                _context.Add(dealers);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewData["HumanId"] = new SelectList(_context.Humans, "Id", "Id", dealers.HumanId);
            ViewData["StoreId"] = new SelectList(_context.Stores, "Id", "Id", dealers.StoreId);
            return View(dealers);
        }

        // GET: Dealers/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var dealers = await _context.Dealers.SingleOrDefaultAsync(m => m.Id == id);
            if (dealers == null)
            {
                return NotFound();
            }
            ViewData["HumanId"] = new SelectList(_context.Humans, "Id", "Id", dealers.HumanId);
            ViewData["StoreId"] = new SelectList(_context.Stores, "Id", "Id", dealers.StoreId);
            return View(dealers);
        }

        // POST: Dealers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,FruitsLoaned,Name,StoreId,HumanId")] Dealers dealers)
        {
            if (id != dealers.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(dealers);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DealersExists(dealers.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            ViewData["HumanId"] = new SelectList(_context.Humans, "Id", "Id", dealers.HumanId);
            ViewData["StoreId"] = new SelectList(_context.Stores, "Id", "Id", dealers.StoreId);
            return View(dealers);
        }

        // GET: Dealers/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var dealers = await _context.Dealers
                .Include(d => d.Human)
                .Include(d => d.Store)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (dealers == null)
            {
                return NotFound();
            }

            return View(dealers);
        }

        // POST: Dealers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var dealers = await _context.Dealers.SingleOrDefaultAsync(m => m.Id == id);
            _context.Dealers.Remove(dealers);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool DealersExists(int id)
        {
            return _context.Dealers.Any(e => e.Id == id);
        }
    }
}