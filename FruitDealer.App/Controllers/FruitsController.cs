using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FruitDealer.App.Models;

namespace FruitDealer.App.Controllers
{
    public class FruitsController : Controller
    {
        private readonly EfFruitDealerContext _context;

        public FruitsController(EfFruitDealerContext context)
        {
            _context = context;    
        }

        // GET: Fruits
        public async Task<IActionResult> Index()
        {
            var efFruitDealerContext = _context.Fruits.Include(f => f.Store);
            return View(await efFruitDealerContext.ToListAsync());
        }

        // GET: Fruits/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var fruits = await _context.Fruits
                .Include(f => f.Store)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (fruits == null)
            {
                return NotFound();
            }

            return View(fruits);
        }

        // GET: Fruits/Create
        public IActionResult Create()
        {
            ViewData["StoreId"] = new SelectList(_context.Stores, "Id", "StoreName");
            return View();
        }

        // POST: Fruits/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,BadDate,BoughtDate,Color,StoreId,FruitTypes")] Fruits fruits)
        {
            if (ModelState.IsValid)
            {
                _context.Add(fruits);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewData["StoreId"] = new SelectList(_context.Stores, "Id", "StoreName", fruits.StoreId);
            return View(fruits);
        }

        // GET: Fruits/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var fruits = await _context.Fruits.SingleOrDefaultAsync(m => m.Id == id);
            if (fruits == null)
            {
                return NotFound();
            }
            ViewData["StoreId"] = new SelectList(_context.Stores, "Id", "StoreName", fruits.StoreId);
            return View(fruits);
        }

        // POST: Fruits/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,BadDate,BoughtDate,Color,StoreId,FruitTypes")] Fruits fruits)
        {
            if (id != fruits.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(fruits);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!FruitsExists(fruits.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            ViewData["StoreId"] = new SelectList(_context.Stores, "Id", "StoreName", fruits.StoreId);
            return View(fruits);
        }

        // GET: Fruits/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var fruits = await _context.Fruits
                .Include(f => f.Store)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (fruits == null)
            {
                return NotFound();
            }

            return View(fruits);
        }

        // POST: Fruits/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var fruits = await _context.Fruits.SingleOrDefaultAsync(m => m.Id == id);
            _context.Fruits.Remove(fruits);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool FruitsExists(int id)
        {
            return _context.Fruits.Any(e => e.Id == id);
        }
    }
}
