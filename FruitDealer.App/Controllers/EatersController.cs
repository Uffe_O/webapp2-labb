using FruitDealer.App.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace FruitDealer.App.Controllers
{
    public class EatersController : Controller
    {
        private readonly EfFruitDealerContext _context;

        public EatersController(EfFruitDealerContext context)
        {
            _context = context;
        }

        // GET: Eaters
        public async Task<IActionResult> Index()
        {
            var fruitDealerContext = _context.Eaters.Include(e => e.Human);
            return View(await fruitDealerContext.ToListAsync());
        }

        // GET: Eaters/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var eaters = await _context.Eaters
                .Include(e => e.Human)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (eaters == null)
            {
                return NotFound();
            }

            return View(eaters);
        }

        // GET: Eaters/Create
        public IActionResult Create()
        {
            ViewData["HumanId"] = new SelectList(_context.Humans, "Id", "Id");
            return View();
        }

        // POST: Eaters/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,HumanId,Healthy")] Eaters eaters)
        {
            if (ModelState.IsValid)
            {
                _context.Add(eaters);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewData["HumanId"] = new SelectList(_context.Humans, "Id", "Id", eaters.HumanId);
            return View(eaters);
        }

        // GET: Eaters/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var eaters = await _context.Eaters.SingleOrDefaultAsync(m => m.Id == id);
            if (eaters == null)
            {
                return NotFound();
            }
            ViewData["HumanId"] = new SelectList(_context.Humans, "Id", "Id", eaters.HumanId);
            return View(eaters);
        }

        // POST: Eaters/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,HumanId,Healthy")] Eaters eaters)
        {
            if (id != eaters.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(eaters);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!EatersExists(eaters.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            ViewData["HumanId"] = new SelectList(_context.Humans, "Id", "Id", eaters.HumanId);
            return View(eaters);
        }

        // GET: Eaters/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var eaters = await _context.Eaters
                .Include(e => e.Human)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (eaters == null)
            {
                return NotFound();
            }

            return View(eaters);
        }

        // POST: Eaters/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var eaters = await _context.Eaters.SingleOrDefaultAsync(m => m.Id == id);
            _context.Eaters.Remove(eaters);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool EatersExists(int id)
        {
            return _context.Eaters.Any(e => e.Id == id);
        }
    }
}