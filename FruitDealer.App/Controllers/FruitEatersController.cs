using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FruitDealer.App.Models;

namespace FruitDealer.App.Controllers
{
    public class FruitEatersController : Controller
    {
        private readonly EfFruitDealerContext _context;

        public FruitEatersController(EfFruitDealerContext context)
        {
            _context = context;    
        }

        // GET: FruitEaters
        public async Task<IActionResult> Index()
        {
            var efFruitDealerContext = _context.FruitEaters.Include(f => f.Eater).Include(f => f.Fruit);
            return View(await efFruitDealerContext.ToListAsync());
        }

        // GET: FruitEaters/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var fruitEaters = await _context.FruitEaters
                .Include(f => f.Eater)
                .Include(f => f.Fruit)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (fruitEaters == null)
            {
                return NotFound();
            }

            return View(fruitEaters);
        }

        // GET: FruitEaters/Create
        public IActionResult Create()
        {
            ViewData["EaterId"] = new SelectList(_context.Eaters, "Id", "Id");
            ViewData["FruitId"] = new SelectList(_context.Fruits, "Id", "Id");
            return View();
        }

        // POST: FruitEaters/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("EaterId,FruitId,Id")] FruitEaters fruitEaters)
        {
            if (ModelState.IsValid)
            {
                _context.Add(fruitEaters);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewData["EaterId"] = new SelectList(_context.Eaters, "Id", "Id", fruitEaters.EaterId);
            ViewData["FruitId"] = new SelectList(_context.Fruits, "Id", "Id", fruitEaters.FruitId);
            return View(fruitEaters);
        }

        // GET: FruitEaters/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var fruitEaters = await _context.FruitEaters.SingleOrDefaultAsync(m => m.Id == id);
            if (fruitEaters == null)
            {
                return NotFound();
            }
            ViewData["EaterId"] = new SelectList(_context.Eaters, "Id", "Id", fruitEaters.EaterId);
            ViewData["FruitId"] = new SelectList(_context.Fruits, "Id", "Id", fruitEaters.FruitId);
            return View(fruitEaters);
        }

        // POST: FruitEaters/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("EaterId,FruitId,Id")] FruitEaters fruitEaters)
        {
            if (id != fruitEaters.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(fruitEaters);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!FruitEatersExists(fruitEaters.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            ViewData["EaterId"] = new SelectList(_context.Eaters, "Id", "Id", fruitEaters.EaterId);
            ViewData["FruitId"] = new SelectList(_context.Fruits, "Id", "Id", fruitEaters.FruitId);
            return View(fruitEaters);
        }

        // GET: FruitEaters/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var fruitEaters = await _context.FruitEaters
                .Include(f => f.Eater)
                .Include(f => f.Fruit)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (fruitEaters == null)
            {
                return NotFound();
            }

            return View(fruitEaters);
        }

        // POST: FruitEaters/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var fruitEaters = await _context.FruitEaters.SingleOrDefaultAsync(m => m.Id == id);
            _context.FruitEaters.Remove(fruitEaters);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool FruitEatersExists(int id)
        {
            return _context.FruitEaters.Any(e => e.Id == id);
        }
    }
}
