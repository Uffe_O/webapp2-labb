using FruitDealer.App.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace FruitDealer.App.Controllers
{
    public class HumansController : Controller
    {
        private readonly EfFruitDealerContext _context;

        public HumansController(EfFruitDealerContext context)
        {
            _context = context;
        }

        // GET: Humans
        public async Task<IActionResult> Index()
        {
            return View(await _context.Humans.ToListAsync());
        }

        // GET: Humans/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var humans = await _context.Humans
                .SingleOrDefaultAsync(m => m.Id == id);
            if (humans == null)
            {
                return NotFound();
            }

            return View(humans);
        }

        // GET: Humans/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Humans/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Economy,Name")] Humans humans)
        {
            if (ModelState.IsValid)
            {
                _context.Add(humans);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(humans);
        }

        // GET: Humans/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var humans = await _context.Humans.SingleOrDefaultAsync(m => m.Id == id);
            if (humans == null)
            {
                return NotFound();
            }
            return View(humans);
        }

        // POST: Humans/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Economy,Name")] Humans humans)
        {
            if (id != humans.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(humans);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!HumansExists(humans.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            return View(humans);
        }

        // GET: Humans/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var humans = await _context.Humans
                .SingleOrDefaultAsync(m => m.Id == id);
            if (humans == null)
            {
                return NotFound();
            }

            return View(humans);
        }

        // POST: Humans/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var humans = await _context.Humans.SingleOrDefaultAsync(m => m.Id == id);
            _context.Humans.Remove(humans);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool HumansExists(int id)
        {
            return _context.Humans.Any(e => e.Id == id);
        }
    }
}